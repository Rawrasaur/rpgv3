﻿using Mirror;
using Mirror.Examples.Basic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : NetworkBehaviour
{

    public float movementSpeed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;
    private Rigidbody playerRigidbody;
    private PlayerCamera playerCamera;

    [SerializeField] float axisH;
    [SerializeField] float axisV;
    [SerializeField] string axisDir;
    [SerializeField] string movementDir;
    [SerializeField] Animator animator;
    [SerializeField] GameObject slashObject;
    public GameObject abilitySpawnPoint;
    [SerializeField] GameObject abilityContainer;

    [SerializeField] NetworkAnimator networkAnimator;

    public bool isDead => health <= 0;

    [SyncVar]
    [SerializeField] int health;

    [SerializeField] TextMesh healthText;

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        abilityContainer = GameObject.FindGameObjectWithTag("AbilityContainer");
        //networkAnimator = GetComponent<NetworkAnimator>();

        if (!hasAuthority) { return; }
        //figure out spawn point in the Network Manager        
        SetLocalCamera();
    }

    [SerializeField] private Vector3 movement = new Vector3();

    void Update()
    {
        healthText.text = health.ToString();
        healthText.transform.rotation = Camera.main.transform.rotation;

        if (!isLocalPlayer) { return; }
        SetInputAxis();
        CalcMovementDirection();
        Move();
        Rotate();

        if (Input.GetKeyDown("1"))
        //(Input.GetMouseButtonDown(0))
        {
            CmdPlayerAttack();
        }

        if (axisH == 0f && axisV == 0f)
        {
            animator.SetBool("Running", false);
            animator.SetBool("ReverseRun", false);
        }

        //Set local players name color to green  
        healthText.color = Color.green;

    }


    [Client]
    void SetLocalCamera()
    {
        playerCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerCamera>();
        playerCamera.SetTarget(this.transform);
    }

    void SetInputAxis()
    {
        axisH = Input.GetAxis("Horizontal");
        axisV = Input.GetAxis("Vertical");
    }

    void Move()
    {
        var inputDirection = new Vector3(axisH, 0, axisV);
        transform.position += inputDirection * Time.deltaTime * movementSpeed;

        if (movementDir == "Forwards")
        {
            animator.SetBool("ReverseRun", false);
            animator.SetBool("Running", true);
        }
        else if (movementDir == "Backwards")
        {
            animator.SetBool("Running", false);
            animator.SetBool("ReverseRun", true);
        }           

    }

    void Rotate()
    {
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, 5 * Time.deltaTime);
        }
    }

    void CalcMovementDirection()
    {
        var fwdX = transform.forward.x;
        var fwdZ = transform.forward.z;

        var fwdXcalc = fwdX * fwdX;
        var fwdZcalc = fwdZ * fwdZ;

        axisDir = (fwdXcalc > fwdZcalc) ? "H" : "V";

        if (axisDir == "H")
        {
            movementDir = ((int)Mathf.Sign(fwdX) * (int)Mathf.Sign(axisH) < 0) ? "Backwards" : "Forwards";
        }
        else if (axisDir == "V")
        {
            movementDir = ((int)Mathf.Sign(fwdZ) * (int)Mathf.Sign(axisV) < 0) ? "Backwards" : "Forwards";
        }
        //Add strafe animation into this when possible
    }

    [Command]
    void CmdPlayerAttack()
    {
        Vector3 spawnPos = gameObject.transform.rotation.eulerAngles;
        var ability = Instantiate(slashObject, abilitySpawnPoint.transform.position, Quaternion.Euler(0, spawnPos.y + 60, 0));
        ability.GetComponent<Ability>().SetSource(gameObject);
        NetworkServer.Spawn(ability);
        RpcOnPlayerAttack();        
    }

    [ClientRpc]
    void RpcOnPlayerAttack()
    {
        animator.SetTrigger("BasicAttack");       
    }

    [ServerCallback]
    public void SetHealth(int amount)
    {
        health += amount;
    }
}
