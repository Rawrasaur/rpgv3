﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Transform target;
    public float distance;
    [SerializeField] bool targetSet;

    // Update is called once per frame
    void Update()
    {
        if (!targetSet || target == null) { return; }
        transform.position = new Vector3(target.position.x, target.position.y + 10, target.position.z - 10);
        
    }

    public void SetTarget(Transform t)
    {
        target = t;
        targetSet = true;
    } 
}
