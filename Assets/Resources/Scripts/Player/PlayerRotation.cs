﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    public Camera MainCamera;
    public float Speed;

    private Vector3 mousePosition;
    private Vector3 direction;
    private float distanceFromObject;
    [SerializeField] float yRotation;
    [SerializeField] float xRotation;
    [SerializeField] float zRotation;

    // Use this for initialization
    void Start()
    {
        MainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameObject.GetComponentInParent<NetworkIdentity>().isLocalPlayer) { return; }
        PlayerRotate();
    }
    void PlayerRotate()
    {
        ////Grab the current mouse position on the screen
        //mousePosition = MainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y - MainCamera.transform.position.y, Input.mousePosition.z ));

        ////Rotates toward the mouse
        //this.transform.eulerAngles = new Vector3(xRotation, yRotation + (Mathf.Atan2((mousePosition.z - transform.position.z), (mousePosition.x - transform.position.x)) * Mathf.Rad2Deg - 90), zRotation);

        //Get the Screen positions of the object
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        //Get the Screen position of the mouse
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        //Get the angle between the points
        float angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        //Ta Daaa
        transform.rotation = Quaternion.Euler(new Vector3(xRotation, (angle * -1) +- 90, zRotation));
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }
}
