﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Ability : NetworkBehaviour
{

    /// <summary>
    /// This is the base class for combat abilities (Melee, Magic and Ranged)
    /// 
    /// Melee, magic, and ranged abilities derive from the same base concept. 
    /// They all act as abstracted entities from the player model. 
    /// For example in melee the player’s weapon does not have any specific colliders attached to it, 
    /// but rather a melee slash animation will appear and act as the visual guide to the attack’s area of effect.
    /// 
    /// 
    /// </summary>
            
    [SerializeField] protected string abilityName;
    [SerializeField] protected string abilityType;
    [SerializeField] protected GameObject abilityObject;
    [SerializeField] protected int damage;
    [SerializeField] protected int cooldownTime;
    [SerializeField] protected GameObject caster;
    [SerializeField] protected GameObject source;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSource(GameObject sourceObject)
    {
        source = sourceObject;
    }
}
