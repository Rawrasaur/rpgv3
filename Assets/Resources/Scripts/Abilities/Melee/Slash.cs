﻿using System.Collections;
using System.Collections.Generic;
//using System.Numerics;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using Mirror;

public class Slash : Ability
{

    /// <summary>
    /// Slash is a basic melee ability that originates at the player's position 
    /// and performs a short/melee range frontal attack
    /// 
    /// Sequence of events:
    /// Spawn ability animation
    /// Spawn ability collider
    /// Perform slash like movement/rotation
    /// Get hit enemies by collider
    /// Calculate/apply damage
    /// 
    /// Properties inherited from the Ability class:
    /// [SerializeField] string AbilityName;
    /// [SerializeField] string AbilityType;
    /// [SerializeField] GameObject AbilityObject;
    /// [SerializeField] int Damage;
    /// [SerializeField] int CooldownTime;
    /// </summary>

    [SerializeField] Vector3 rotationEnd;
    [SerializeField] Vector3 rotationReset;
    [SerializeField] Quaternion rotationStart;
    [SerializeField] Vector3 rotationStartVector;
    [SerializeField] float rotationSpeed;
    [SerializeField] bool isRotating;
    [SerializeField] bool slashComplete;
    [SerializeField] float startTime;
    [SerializeField] float totalDist;
    [SerializeField] TrailRenderer trailRend;


    void Start()
    {        
        rotationStart = gameObject.transform.rotation;
        rotationStartVector = gameObject.transform.rotation.eulerAngles;
        rotationEnd = new Vector3 (0,rotationStartVector.y - 120,0);
        isRotating = false;
        slashComplete = true;
        ExecuteAbility();
    }
    
    void Update()
    {
       // gameObject.transform.position = source.GetComponent<PlayerController>().abilitySpawnPoint.transform.position;
        
        if (!isRotating) { return; }
        Rotate();
    }

    
    public void ExecuteAbility()
    {
        castAbility();
    }

    
    void castAbility()
    {
        // Perform slash like movement/rotation
        // 1. Set collider active
        if (isRotating) return;
        EnableAbilityCollider();
        trailRend.emitting = true;

        // 2. Rotate parent object from right to left
        isRotating = true;
        slashComplete = false;
        totalDist = (Vector3.Distance(transform.rotation.eulerAngles, rotationEnd));       
        startTime = Time.time;

        StartCoroutine(rotateSlash());

        // 3. For each collision with an enemy add to list
        // Save hit enemies by collider
    }
        
    [Server]
    void EnableAbilityCollider()
    {
        gameObject.GetComponent<CapsuleCollider>().enabled = true;
    }

    void Rotate()
    {
        if (!slashComplete)
        {
            float distCovered = (Time.time - startTime) * rotationSpeed;
            float fractionOfJourney = distCovered / totalDist;
            //transform.rotation = Quaternion.Lerp(Quaternion.Euler(rotationReset), Quaternion.Euler(rotationEnd), fractionOfJourney);

            transform.rotation = Quaternion.Lerp(rotationStart, Quaternion.Euler(rotationEnd), fractionOfJourney);

            if (fractionOfJourney >= 1f)
            {
                slashComplete = true;
            }
        }
    }

    [Server]
    IEnumerator rotateSlash()
    {
        yield return new WaitUntil(() => slashComplete);        
        resolveAbility();
    }

    [ServerCallback]
    void resolveAbility()
    {
        NetworkServer.Destroy(gameObject);
        //Get hit enemies by collider
        //Resolve damage    
        //gameObject.GetComponent<CapsuleCollider>().enabled = false;
        //trailRend.emitting = false;
        //gameObject.transform.localEulerAngles = rotationReset;
        //isRotating = false;
    }

    [ServerCallback]
    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Resolving collision with: " + col.gameObject.name);

        if (col.gameObject.tag == "Player" && col.gameObject != source) 
        {

        // || col.gameObject.tag != "Enemy"

        resolveCollision(col.gameObject);
        }
        
    }

    [ServerCallback]
    void resolveCollision(GameObject collider)
    {
        //call other player or enemy to do health stuff
        collider.GetComponent<PlayerController>().SetHealth(damage);

        Debug.Log("Resolving collision with: " + collider.gameObject.name);
    }

}
